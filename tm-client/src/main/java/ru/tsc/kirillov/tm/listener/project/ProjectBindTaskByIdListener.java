package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectBindTaskByIdRequest;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectBindTaskByIdListener extends AbstractProjectTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "project-bind-task-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Привязать задачу к проекту.";
    }

    @Override
    @EventListener(condition = "@projectBindTaskByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Привязка задачи к проекту]");
        System.out.println("Введите ID проекта:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("Введите ID задачи:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final ProjectBindTaskByIdRequest request = new ProjectBindTaskByIdRequest(getToken(), projectId, taskId);
        getProjectTaskEndpoint().bindTaskToProjectId(request);
    }

}
