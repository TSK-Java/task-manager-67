package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectGetByIdRequest;
import ru.tsc.kirillov.tm.dto.response.ProjectGetByIdResponse;
import ru.tsc.kirillov.tm.event.ConsoleEvent;
import ru.tsc.kirillov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectShowListener {

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить проект по его ID.";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Отображение проекта по ID]");
        System.out.println("Введите ID проекта:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken(), id);
        @NotNull final ProjectGetByIdResponse response = getProjectEndpoint().getProjectById(request);
        showProject(response.getProject());
    }

}
