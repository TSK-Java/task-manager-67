package ru.tsc.kirillov.tm.listener.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
