package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kirillov.tm.api.ProjectRestEndpoint;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.service.ProjectService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return projectService
                .findAll()
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findById/{id}")
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsById(id);
    }

    @Override
    @PostMapping("/save")
    public ProjectDto save(@NotNull @RequestBody final ProjectDto project) {
        return projectService.save(project);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final ProjectDto project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<ProjectDto> projects) {
        projectService.remove(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        projectService.removeById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

}
