package ru.tsc.kirillov.tm.api.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

public interface ITaskDtoRepository extends JpaRepository<TaskDto, String> {

}
