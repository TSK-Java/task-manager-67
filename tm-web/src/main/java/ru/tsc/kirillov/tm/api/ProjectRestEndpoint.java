package ru.tsc.kirillov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

import java.util.List;

@RequestMapping("/api/projects")
public interface ProjectRestEndpoint {

    @GetMapping("/findAll")
    List<ProjectDto> findAll();

    @GetMapping("/findById/{id}")
    ProjectDto findById(@NotNull @PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@NotNull @PathVariable("id") String id);

    @PostMapping("/save")
    ProjectDto save(@NotNull @RequestBody ProjectDto project);

    @PostMapping("/delete")
    void delete(@NotNull @RequestBody ProjectDto project);

    @PostMapping("/deleteAll")
    void clear(@NotNull @RequestBody List<ProjectDto> projects);

    @DeleteMapping("/clear")
    void clear();

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @GetMapping("/count")
    long count();

}
