package ru.tsc.kirillov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kirillov.tm.api.TaskRestEndpoint;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.service.TaskService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskService
                .findAll()
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findById/{id}")
    public TaskDto findById(@NotNull @PathVariable("id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskService.existsById(id);
    }

    @Override
    @PostMapping("/save")
    public TaskDto save(@NotNull @RequestBody final TaskDto task) {
        return taskService.save(task);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final TaskDto task) {
        taskService.remove(task);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear(@NotNull @RequestBody final List<TaskDto> tasks) {
        taskService.remove(tasks);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear();
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        taskService.removeById(id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }
    
}
