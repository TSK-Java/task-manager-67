package ru.tsc.kirillov.tm.dto.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import ru.tsc.kirillov.tm.api.model.IWBS;
import ru.tsc.kirillov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractWbsDtoModel extends AbstractAuditDtoModel implements IWBS {

    @NotNull
    protected static final String FORMAT_DATE_UI = "yyyy-MM-dd";

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Column
    @Nullable
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "date_begin")
    @DateTimeFormat(pattern = FORMAT_DATE_UI)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE_ISO_8601)
    private Date dateBegin;

    @Nullable
    @Column(name = "date_end")
    @DateTimeFormat(pattern = FORMAT_DATE_UI)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = FORMAT_DATE_ISO_8601)
    private Date dateEnd;

    public AbstractWbsDtoModel(
            @NotNull final String name
    ) {
        super();
        this.name = name;
    }

    public AbstractWbsDtoModel(
            @NotNull final String name,
            @Nullable final String description
    ) {
        super();
        this.name = name;
        this.description = description;
    }

    public AbstractWbsDtoModel(
            @NotNull final String name,
            @NotNull final Status status,
            @Nullable final Date dateBegin
    ) {
        super();
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    public AbstractWbsDtoModel(
            @NotNull final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        super();
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public String toString() {
        return name + " : " + getStatus().getDisplayName() + " : " + description;
    }

}
