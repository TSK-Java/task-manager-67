package ru.tsc.kirillov.tm.dto.soap.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "id"
})
@XmlRootElement(name = "projectDeleteByIdRequest")
public class ProjectDeleteByIdRequest {

    @XmlElement(required = true)
    protected String id;

}
