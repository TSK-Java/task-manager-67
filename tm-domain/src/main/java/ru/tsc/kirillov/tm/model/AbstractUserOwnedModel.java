package ru.tsc.kirillov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractUserOwnedModel extends AbstractAuditModel {

    @Nullable
    @ManyToOne
    private User user;

    public AbstractUserOwnedModel(@Nullable final User user) {
        this.user = user;
    }

}
