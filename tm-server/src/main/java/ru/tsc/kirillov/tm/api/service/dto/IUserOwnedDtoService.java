package ru.tsc.kirillov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.AbstractUserOwnedDtoModel;

import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedDtoModel> extends IDtoService<M> {

    void clear(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@Nullable String userId, @Nullable Integer index);

    long count(@Nullable String userId);

}
